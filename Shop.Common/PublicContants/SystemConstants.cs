﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.PublicContants
{
    public class SystemConstants
    {
        public class Roles
        {
            public const string ROLE_ADMIN = "admin";
            public const string ROLE_USER = "user";
            public const string ROLE_NHANVIEN = "nhanvien";
        }
    }
}
