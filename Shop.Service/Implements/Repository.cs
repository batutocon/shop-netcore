﻿using Microsoft.EntityFrameworkCore;
using Shop.Common.ViewModels.Response;
using Shop.Model;
using Shop.Model.Entities;
using Shop.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Service.Implements
{
    public class Repository<T> : IRepository<T> where T : class
    {
        DbSet<T> _dbSet;
        private ShopContext _context;

        public Repository(ShopContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public IEnumerable<T> GetAllRecords()
        {
            return _dbSet.ToList();
        }

        public IQueryable<T> GetAllRecordsIQueryable()
        {
            return _dbSet;
        }

        public IEnumerable<T> GetRecordsToShow(int pageNo, int pageSize, int currentPageNo, Expression<Func<T, bool>> wherePredict, Expression<Func<T, int>> orderByPredict)
        {
            if (wherePredict != null)
                return _dbSet.OrderBy(orderByPredict).Where(wherePredict).ToList();
            else
                return _dbSet.OrderBy(orderByPredict).ToList();
        }

        public int GetAllRecordsCount()
        {
            return _dbSet.Count();
        }

        public void Add(T entity)
        {
            try
            {
                _dbSet.Add(entity);
                //_context.SaveChanges();
            }
            catch (Exception e)
            {
                //foreach (var eve in e.EntityValidationErrors)
                //{
                //    foreach (var ve in eve.ValidationErrors)
                //    {

                //    }
                //}
                //throw;
            }

        }

        /// <summary>
        /// Updates table entity passed to it
        /// </summary>
        /// <param name="entity"></param>
        public void Update(T entity)
        {
            _dbSet.Attach(entity);
            if (_context.Entry(entity).State == EntityState.Modified)
            {
                _context.Entry(entity).State = EntityState.Added;
                _context.Entry(entity).State = EntityState.Modified;
            }
            _context.Entry(entity).State = EntityState.Modified;
        }
        public void DeAttach(T entity)
        {
            _context.Entry(entity).State = EntityState.Detached;
        }
        public void UpdateByWhereClause(Expression<Func<T, bool>> wherePredict, Action<T> forEachPredict)
        {
            _dbSet.Where(wherePredict).ToList().ForEach(forEachPredict);
        }

        public T GetFirstOrDefault(int recordId)
        {
            return _dbSet.Find(recordId);
        }

        public T GetFirstOrDefaultByParameter(Expression<Func<T, bool>> wherePredict)
        {
            return _dbSet.Where(wherePredict).FirstOrDefault();
        }

        public IEnumerable<T> GetListByParameter(Expression<Func<T, bool>> wherePredict)
        {
            return _dbSet.Where(wherePredict).ToList();
        }

        public void Remove(T entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
                _dbSet.Attach(entity);
            _dbSet.Remove(entity);
        }

        public void RemoveByWhereClause(Expression<Func<T, bool>> wherePredict)
        {
            T entity = _dbSet.Where(wherePredict).FirstOrDefault();
            Remove(entity);
        }

        public void RemoveRangeByWhereClause(Expression<Func<T, bool>> wherePredict)
        {
            List<T> entity = _dbSet.Where(wherePredict).ToList();
            foreach (var ent in entity)
            {
                Remove(ent);
            }
        }
        public void DeleteMarkByWhereClause(Expression<Func<T, bool>> wherePredict, Action<T> ForEachPredict)
        {
            _dbSet.Where(wherePredict).ToList().ForEach(ForEachPredict);
            _context.SaveChanges();
        }

        public void InactiveAndDeleteMarkByWhereClause(Expression<Func<T, bool>> wherePredict, Action<T> ForEachPredict)
        {
            _dbSet.Where(wherePredict).ToList().ForEach(ForEachPredict);
            _context.SaveChanges();
        }

        /// <summary>
        /// Returns result by where clause in descending order
        /// </summary>
        /// <param name="orderByPredict"></param>
        /// <returns></returns>
        public IQueryable<T> OrderByDescending(Expression<Func<T, int>> orderByPredict)
        {
            if (orderByPredict == null)
            {
                return _dbSet;
            }
            return _dbSet.OrderByDescending(orderByPredict);
        }

        public IEnumerable<T> GetResultBySqlProcedure(string query, params object[] parameters)
        {
            if (parameters != null)
            {
                IEnumerable<T> rs = (IEnumerable<T>)_context.Set<T>().FromSqlRaw(query, parameters).ToList();
                return rs;
            }
            // return _context.BookingOfficesDaCoTrip.FromSqlRaw(query, parameters).ToList();
            else
            {
                IEnumerable<T> rs = (IEnumerable<T>)_context.Set<T>().FromSqlRaw(query, parameters).ToList();
                return rs;
            }
        }

        T IRepository<T>.GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        IQueryable<T> IRepository<T>.Querry()
        {
            return _context.Set<T>();
        }

        IEnumerable<T> IRepository<T>.GetAll()
        {
            return _context.Set<T>().ToList();
        }

        /// <summary>
        /// Executes procedure in database and returns result
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
    }
}
