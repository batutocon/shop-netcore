﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.ViewModels
{
    public class BaseCreate
    {
        public DateTime CreateAt { get; set; }
        public string CreateBy { get; set; }
    }
    public class BaseUpdate
    {
        public DateTime UpdateAt { get; set; }
        public string UpdateBy { get; set; }
    }
}
