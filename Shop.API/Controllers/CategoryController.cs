﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shop.Common.PublicContants;
using Shop.Common.ViewModels;
using Shop.Common.ViewModels.Response;
using Shop.Model.Entities;
using Shop.Service.UnitOfWork;

namespace Shop.API.Controllers
{
    [ApiController]
    //[Authorize(Roles = SystemConstants.Roles.ROLE_ADMIN+","+SystemConstants.Roles.ROLE_NHANVIEN)]
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        readonly UnitOfWork _ouw;
        public CategoryController(UnitOfWork ouw)
        {
            _ouw = ouw;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _ouw.ICategory.GetAllRecords().ToList();
            return Ok(lst);
        }

        [HttpPost]
        public ActionResult Create([FromForm] CategoryCreate request)
        {
            var obj = new Category();
            obj.Name = request.Name;
            obj.CreateBy = request.CreateBy;
            obj.CreateAt = request.CreateAt;
            try
            {
                _ouw.ICategory.Add(obj);
                _ouw.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Create thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Create thất bại" });
            }
        }

        [HttpPut]
        public ActionResult Update([FromForm] CategoryUpdate request)
        {
            var obj = _ouw.ICategory.GetFirstOrDefault(request.Id);
            if (obj == null)
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Không tìm thấy bản ghi" });
            }
            else
            {
                obj.Name = request.Name;
                obj.UpdateAt = request.UpdateAt;
                obj.UpdateBy = request.UpdateBy;
            }
            try
            {
                _ouw.ICategory.Update(obj);
                _ouw.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Update thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Update thất bại" });
            }
        }

        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            var obj = _ouw.ICategory.GetById(Id);
            try
            {
                _ouw.ICategory.Remove(obj);
                _ouw.SaveChanges();
                return Ok(new ApiResult() { IsSuccessed = true, Message = "Delete thành công" });

            }
            catch
            {
                return Ok(new ApiResult() { IsSuccessed = false, Message = "Delete thất bại" });
            }
        }
    }
}
