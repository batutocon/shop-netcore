﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.ViewModels
{
    public class CategoryCreate : BaseCreate
    {
        public string Name { get; set; }
    }
    public class CategoryUpdate : BaseUpdate
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
