﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Common.PublicContants;

namespace Shop.Model.Entities
{
    public class Product : BaseEnitity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Count { get; set; }
        public int IsSale { get; set; }
        public ProductStatus Status { get; set; }
    }
}
