﻿using Shop.Common.PublicContants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Model.Entities
{
    public class OrderDetail : BaseEnitity
    {
        public int Id { get; set; }
        public List<OrderItem> lstItem { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public OrderDetailStatus Status { get; set; }
    }
}
