﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Model.Entities
{
    public class ProductCate
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int CategorytId { get; set; }
        public Category Category { get; set; }

    }
}
