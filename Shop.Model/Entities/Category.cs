﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Model.Entities
{
    public class Category : BaseEnitity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
