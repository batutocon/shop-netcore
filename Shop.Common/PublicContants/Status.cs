﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.PublicContants
{
    public enum ProductStatus
    {
        NotActive,
        Active,
    }

    public enum OrderDetailStatus
    {
        Create,
        Shipping,
        Complete,
    }
}
