﻿using Shop.Common.ViewModels.Response;
using Shop.Model.Entities;
using Shop.Service.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Service.Interfaces
{
    public interface ICategory : IRepository<Category>
    {
        Task<PageResult<Category>> Filter(string key, string type, int pagesize, int page);
    }
}
