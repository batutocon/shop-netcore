﻿using Shop.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Service.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ICategory ICategory { get; }
        int SaveChanges();       
    }
}
