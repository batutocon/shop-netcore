﻿using Shop.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Service.Interfaces
{
    public interface IUser
    {
        Task<string> Authen(UserLogin rq);
        Task<bool> Register(UserRegister rq);
    }
}
