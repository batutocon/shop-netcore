﻿using Shop.Model;
using Shop.Service.Implements;
using Shop.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Service.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public ShopContext _context;

        public ICategory ICategory { get; private set; }

        public UnitOfWork(ShopContext context)
        {
            _context = context;
            ICategory = new CategoryService(_context);
        }
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
