﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shop.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Model.Configuaration
{
    public class ProductCateConfig : IEntityTypeConfiguration<ProductCate>
    {
        public void Configure(EntityTypeBuilder<ProductCate> builder)
        {
            builder.ToTable("ProductCate");
            builder.HasKey(x => new {x.ProductId, x.CategorytId});
        }
    }
}
