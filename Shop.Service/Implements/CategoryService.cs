﻿using Shop.Common.ViewModels.Response;
using Shop.Model;
using Shop.Model.Entities;
using Shop.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Service.Implements
{
    public class CategoryService : Repository<Category>, ICategory
    {
        public CategoryService(ShopContext context) : base(context)
        {
        }

        public Task<PageResult<Category>> Filter(string key, string type, int pagesize, int page)
        {
            throw new NotImplementedException();
        }
    }
}
