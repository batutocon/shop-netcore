﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop.Common.ViewModels.Response
{
    public class ApiResult
    {
        public bool IsSuccessed { get; set; }
        public string Message { get; set; }
    }
}
